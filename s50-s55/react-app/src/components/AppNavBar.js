import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

import { useState, useEffect, useContext } from 'react';
import { NavLink, Link } from 'react-router-dom';

import UserContext from '../UserContext';

// the ask keyword allows components to be trreated as if they are different component gaining access to it's properties and functionalities
// the to keyword is used in place of the 'href' for providing the URL for the page
export default function AppNavBar() {

    const { user } = useContext(UserContext);

    // In herem  will capture the value from our localStorage and then store it as a state.
    // Syntax:
        // localStorage.getItem('key/property';)

    // const [user, setUser] = useState(localStorage.getItem('email'));

    // const localStorage = localStorage.getItem('email')

    /*useEffect(() => {
        setUser(localStorage.getItem('email'))
    }, [localStorage.getItem('email')])*/

    return (
        <Navbar bg="primary" expand="lg">
            <Container fluid>
                <Navbar.Brand as = {Link} to = '/'>Zuitt Booking</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                    <Nav.Link as = {Link} to = '/'>Home</Nav.Link>
                    <Nav.Link as = {Link} to = '/courses'>Courses</Nav.Link>
                    <Nav.Link as = {Link} to = '/register'>Register</Nav.Link>
                    {
                        user.id === null || user.id === undefined
                        ?
                        <Nav.Link as = {Link} to = '/login'>Login</Nav.Link>
                        :
                        <Nav.Link as = {Link} to = '/logout'>Logout</Nav.Link>
                    }

                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>

    )
}

// import Button from 'react-bootstrap/Button';
// import Form from 'react-bootstrap/Form';

import { Button, Form, Row, Col} from 'react-bootstrap'
// we need to import the useState from the react
import { useState, useEffect, useContext} from 'react';
import { Navigate, useNavigate } from 'react-router-dom'; 

import Swal2 from 'sweetalert2';

import UserContext from '../UserContext';


export default function Register(){
	// State the hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [password2, setPassword2] = useState('');
	const [isPassed, setIsPassed] = useState(true);
	const [mobileNoPassed, setMobileNoPassed] = useState(true);
	const [isDisabled, setIsDisabled] = useState(true);

	// Allows us to consume the UserContext object and it's properties to use for validation
	const {user, setUser} = useContext(UserContext)	

	const navigate = useNavigate();

	// when the email changes it will have a side effect that will console its value
	useEffect(() => {
		// console.log(email);
		if(email.length > 15){
			setIsPassed(false);
		} else {
			setIsPassed(true);
		}
	}, [email]);

	// effect for mobile no. to contain only 11 numbers
	useEffect(() => {
		if(mobileNo.length !== 11 ){
			setMobileNoPassed(false);
		} else {
			setMobileNoPassed(true);
		}
	}, [mobileNo]);

	// this useEffect will disable or enable our sign up button
	useEffect(() => {
		// we are going to add if statement and all the condition that we mention should be satisfied before we enable the sign up button
		if(firstName !== '' && lastName !== '' && mobileNo !== '' &&  email !== '' && password !== '' && password2 !== '' && password === password2 && email.length <= 15 && mobileNo.length == 11){
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [firstName, lastName, mobileNo, email, password, password2]);

	// function to simulate user registration
	function registerUser(event) {
		// prevent page reloading
		event.preventDefault();

		/*alert('Thank you for registering!');

		setEmail('');
		setPassword1('');
		setPassword2('');*/

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(response => response.json())
		.then(data => {
			/*console.log(data);*/

			if(data === false){
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password
					})
				})
				.then(response => response.json())
				.then(data => {
					/*console.log(data);*/
					navigate('/login');
					Swal2.fire({
						title : "Registration successful!",
						icon : 'success'
					})
				})

			} else {
				Swal2.fire({
					title : 'Register Unsuccesful!',
					icon : 'error',
					text: 'Email already exists. Please another one.'
				})

				navigate('/register');
			}			

		})


	}

	return (	
		user.id === null || user.id === undefined
		?
			<Row>
				<Col className = "pt-2 mx-auto">
					<h1 className = "text-center">Register</h1>
					<Form onSubmit = {event => registerUser(event)}>

						  <Form.Group className="col-6 mx-auto mb-3" as={Col} controlId="formFirstName">
						    <Form.Label>First Name</Form.Label>
						    <Form.Control type="text" placeholder="Enter your first name" value = {firstName}
					        	onChange = {event => setFirstName(event.target.value)} />
						  </Form.Group>

						  <Form.Group className="col-6 mx-auto mb-3" as={Col} controlId="formLastName">
						    <Form.Label>Last Name</Form.Label>
						    <Form.Control type="text" placeholder="Enter your last name" value = {lastName}
					        	onChange = {event => setLastName(event.target.value)} />
						  </Form.Group>

						  <Form.Group className="col-6 mx-auto mb-3" as={Col} controlId="formMobileNo">
						    <Form.Label>Mobile Number</Form.Label>
						    <Form.Control type="text" placeholder="Enter your mobile number" value = {mobileNo}
					        	onChange = {event => setMobileNo(event.target.value)} />
					        <Form.Text className="text-muted" hidden = {mobileNoPassed}>
					          The mobile number should contain 11 numbers only!
					        </Form.Text>
						  </Form.Group>

					      <Form.Group className="col-6 mx-auto mb-3" controlId="formBasicEmail">
					        <Form.Label>Email address</Form.Label>
					        <Form.Control 
					        	type="email"
					        	placeholder="Enter email"
					        	value = {email}
					        	onChange = {event => setEmail(event.target.value)}
					        	/>
					        <Form.Text className="text-muted" hidden = {isPassed}>
					          The email should not exceed 15 characters!
					        </Form.Text>
					      </Form.Group>

					      <Form.Group className="col-6 mx-auto mb-3" controlId="formBasicPassword1">
					        <Form.Label>Password</Form.Label>
					        <Form.Control
					        	type="password"
					        	placeholder="Password"
					        	value = {password}
					        	onChange = {event => setPassword(event.target.value)}
					        />
					      </Form.Group>

					      <Form.Group className="col-6 mx-auto mb-3" controlId="formBasicPassword2">
					        <Form.Label>Confirm Password</Form.Label>
					        <Form.Control
					        	type="password"
					        	placeholder="Retype your nominated password"
					        	type="password"
					        	placeholder="Password"
					        	value = {password2}
					        	onChange = {event => setPassword2(event.target.value)}
					        	/>
					      </Form.Group>

					      {/*<Form.Group className="mb-3" controlId="formBasicCheckbox">
					        <Form.Check type="checkbox" label="Check me out" />
					      </Form.Group>*/}

					      <div className="mx-auto col-6 text-center">
					      	<Button variant="primary" type="submit" disabled = {isDisabled}>
					      	  Sign up
					      	</Button>
					      </div>
				    </Form>
				</Col>
			</Row>
		:
		// redirected to PageNotFound if already logged in
		<Navigate to = '/*' />

	);
}
import { Fragment } from "react";
import { Link } from "react-router-dom";

export default function PageNotFound() {
  return (
    <Fragment>
      <div>
      	<h1 className = "mt-4">404</h1>
        <h5>Page Not Found</h5>
        <p>
          Go back to the <Link to="/">homepage</Link>.
        </p>
      </div>
    </Fragment>
  );
}

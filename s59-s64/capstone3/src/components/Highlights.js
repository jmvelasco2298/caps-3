import { Row, Col, Card, Button } from "react-bootstrap";
import { Placeholder } from "react-bootstrap";
export default function Highlights() {
  return (

    <div className="d-flex justify-content-around">
      <Card style={{ width: "25rem" }}>
        <Card.Img
          variant="top"
          src="https://st2.depositphotos.com/1137241/6047/i/450/depositphotos_60470809-stock-photo-vintage-set-of-old-things.jpg"
        />
        <Card.Body>
          <Card.Title>From the past</Card.Title>
          <Card.Text>
            Lost things treasure old accessories and memorabilia that makes people feel how time flies. We collect antique collections and souvenirs from the past such as old sign Plates, VCM players and famously the sought after vinyl records of the old.
          </Card.Text>
        </Card.Body>
      </Card>

      <Card style={{ width: "25rem" }}>
        <Card.Img
          variant="top"
          src="https://ae01.alicdn.com/kf/H59ad32395fe541db83c96f8e4794cc097.jpg_640x640Q90.jpg_.webp"
        />
        <Card.Body>
          <Card.Title>Antique Arts</Card.Title>
          <Card.Text>
            Our shop's system is buy and sell, as we also accept old collections that you want to sell. We know old memories are hard to let go, but in our shop, those memories will live on and passed on to others.
          </Card.Text>
        </Card.Body>
      </Card>

      <Card style={{ width: "25rem" }}>
        <Card.Img
          variant="top"
          src="https://www.avigailadam.com/cdn/shop/products/image_b451cd26-382f-4c27-a16d-d7f9e860d84e_1024x.jpg?v=1614141016"
        />
        <Card.Body>
          <Card.Title>Relive Old Memories</Card.Title>
          <Card.Text>
            As you browse through our collection, some will be familiar to you, some will be strange, some will be new. All these things have in common, is the time and generations they had gone through, together with the memories of people with it.
          </Card.Text>
        </Card.Body>
      </Card>
    </div>
  );
}

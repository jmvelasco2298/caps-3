import { useState, useEffect, useContext } from "react";
import { Container, Row, Col, Card, Button, Form } from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function CourseView() {
  const { user } = useContext(UserContext);

  // allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling a course
  const history = useNavigate();

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [isDisabled, setIsDisabled] = useState(false);
  const [quantity, setQuantity] = useState(1);
  const [image, setImage] = useState("");

  // useParams hook allows use to retrieve the courseId passed via the URL
  const { id } = useParams();

  const purchase = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/addOrder`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`
      },
      body: JSON.stringify({
        productId: `${productId}`,
        quantity: quantity
      })
    })
      .then((res) => res.json())
      .then((data) => {
        // console.log(data);

        if (data) {
          Swal.fire({
            title: "Purchased!",
            icon: "success",
            text: "The sea is calling!"
          });

          history("/products");
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again later"
          });
        }
      });
  };

  useEffect(() => {
    // console.log(courseId);
    fetch(`${process.env.REACT_APP_API_URL}/products/getSingleProduct/${id}`)
      .then((res) => res.json())
      .then((data) => {
        // console.log("Single data");
        // console.log(data);
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setImage(data.image);
      });
  }, []);

  useEffect(() => {
    if (user.isAdmin === true) {
      setIsDisabled(true);
    } else {
      setIsDisabled(false);
    }
  }, [`${user}`]);

  return (
    <Row className="mt-3 mb-3">
      <div class="d-flex justify-content-center">
        <Card style={{ width: "20vw" }}>
          <Card.Img variant="top" src={image} />
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Text>{description}</Card.Text>
            <Card.Text>
              Price: <span>&#x20B1;</span> {price}
            </Card.Text>
            <Form>
              <Form.Group controlId="quantity">
                <Form.Label> Quantity</Form.Label>
                <div className="input-group">
                  <Button
                    variant="outline-secondary"
                    id="btn-minus"
                    onClick={() => setQuantity(quantity - 1)}
                    disabled={quantity === 1}
                  >
                    -
                  </Button>
                  <Form.Control
                    type="number"
                    min={1}
                    value={quantity}
                    onChange={(e) => setQuantity(parseInt(e.target.value))}
                    className="text-center"
                  />
                  <Button
                    variant="outline-secondary"
                    id="btn-plus"
                    onClick={() => setQuantity(quantity + 1)}
                  >
                    +
                  </Button>
                </div>
              </Form.Group>
            </Form>

            {user.id !== null ? (
              <div class="mt-2 text-center">
                <Button
                  disabled={isDisabled}
                  variant="primary"
                  onClick={() => purchase(id)}
                >
                  Purchase
                </Button>
              </div>
            ) : (
              <div class="mt-2 text-center">
                <Link className="btn btn-danger" to="/login">
                  Please Log In First
                </Link>
              </div>
            )}
          </Card.Body>
        </Card>
      </div>
    </Row>
  );
}
